#!/usr/bin/env python
import rospy
from dashgo_bringup.msg import AGVState
from dashgo_bringup.srv import SetAGVQuantity
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import Quaternion

from geometry_msgs.msg import PoseStamped

def setAGVQuantity(data):
    global agv_state
    agv_state.quantity = data.quantity
    return "OK"

agv_state = None

if __name__ == "__main__":
    rospy.init_node("agv_state_publisher")

    # Create Services
    setAGVQuantityService = rospy.Service('set_agv_quantity', SetAGVQuantity, setAGVQuantity)

    # Parameter
    init_pose_x = rospy.get_param('~init_pose_x', 0)
    init_pose_y = rospy.get_param('~init_pose_y', 0)
    init_pose_a = rospy.get_param('~init_pose_a', 0)
    quantity =rospy.get_param('~quantity', 5)

    rate = rospy.Rate(10)

    agv_state_pub = rospy.Publisher("agv_state", AGVState, queue_size=10)
    agv_state = AGVState()
    agv_state.name = rospy.get_namespace()[1:-1]
    agv_state.usable = 1
    agv_state.quantity = quantity
    init_pose = PoseStamped()
    angle = quaternion_from_euler(0, 0, init_pose_a, axes='sxyz')
    orientation = Quaternion(*angle)
    init_pose.pose.position.x = init_pose_x
    init_pose.pose.position.y = init_pose_y
    init_pose.pose.orientation = orientation
    agv_state.init_pose = init_pose

    while not rospy.is_shutdown():
        agv_state_pub.publish(agv_state)
        rate.sleep()

